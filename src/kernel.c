#include "kernel.h"

// TODO: Update colour palette
// TODO: Decide where to log to, serial, screen or both
// TODO: Work on VMM and PMM
// TODO: More features

#define BLACK RGB(0, 0, 0)
#define WHITE RGB(255, 255, 255)

colour_t palette[8] = {
    RGB(88, 110, 117),  /* Black */
    RGB(220, 50, 47),   /* Red */
    RGB(133, 153, 0),   /* Green */
    RGB(181, 137, 0),   /* Yellow */
    RGB(38, 139, 210),  /* Blue */
    RGB(108, 113, 196), /* Magenta */
    RGB(42, 161, 152),  /* Cyan */
    RGB(253, 246, 227)  /* White */
};

void _start(struct stivale2_struct *info) {
  gdt_init();
  idt_init();
  pit_init(1000);
  rtc_init();
  serial_init();
  screen_init(info);
  keyboard_init();

  // Set random
  datetime_t current = rtc_get_datetime();
  srand(current.second);
  log(INFO, "Initialized random; random int: %d", rand());

  text_init(palette, WHITE, BLACK);

  serial_puts("Hello From OS!\n");

  text_puts("Hello From OS!\n");

  __asm__ volatile("int3");

  for (;;) {
    __asm__ volatile("hlt");
  }
}
