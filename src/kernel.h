#ifndef KERNEL_H
#define KERNEL_H

#include <boot/stivale2.h>
#include <devices/keyboard/keyboard.h>
#include <devices/pit/pit.h>
#include <devices/rtc/rtc.h>
#include <devices/screen/screen.h>
#include <devices/screen/text/text.h>
#include <devices/serial/serial.h>
#include <libasm/asm.h>
#include <liblog/logging.h>
#include <librand/random.h>
#include <libstr/string.h>
#include <stddef.h>
#include <stdint.h>
#include <system/gdt/gdt.h>
#include <system/interrupts/idt.h>

#endif
