#ifndef VIDEO_H
#define VIDEO_H

#include <boot/stivale2.h>
#include <devices/serial/serial.h>
#include <liblog/logging.h>
#include <stddef.h>
#include <stdint.h>

#define RGB(r, g, b) ((colour_t){r, g, b})

#define RED_SHIFT 16
#define GREEN_SHIFT 8
#define BLUE_SHIFT 0

typedef struct {
  uint8_t r, g, b;
} colour_t;

colour_t rgb(int r, int g, int b);
uint32_t get_colour(colour_t *colour);
void set_pixel(uint16_t x, uint16_t y, uint32_t colour);
void set_screen(uint32_t colour);
void screen_init(struct stivale2_struct *info);

#endif
