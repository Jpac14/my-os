#ifndef TEXT_H
#define TEXT_H

#include <devices/screen/screen.h>
#include <libmem/memory.h>
#include <libstr/string.h>
#include <stddef.h>
#include <stdint.h>

struct psf {
  uint32_t magic;
  uint32_t version;
  uint32_t headersize;
  uint32_t flags;

  uint32_t numglyph;
  uint32_t glyph_size;
  uint32_t height;
  uint32_t width;

  uint8_t data[];
};

void text_putc(char c, int position_x, int position_y);
void text_put(char c);
void text_puts(char *string);
void text_printf(char *format, ...);
void text_clear(void);
void text_init(colour_t palette[8], colour_t foreground, colour_t background);

#endif
