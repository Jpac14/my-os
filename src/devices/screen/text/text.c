#include "text.h"

extern struct psf fb_font;

static struct {
  colour_t palette[8];
  colour_t default_foreground;
  colour_t foreground;
  colour_t default_background;
  colour_t background;
  enum {
    OFF,
    PRE_PARSING,
    PARSING,
  } parsing_state;
  size_t ptr;
  char nbr[3];
  size_t cursor_x, cursor_y;
} state;

void text_putc(char c, int position_x, int position_y) {
  switch (c) {
    case '\n': {
      state.cursor_y++;
      state.cursor_x = 1;
      return;
    }
    case '\t': {
      state.cursor_x += 4;
      return;
    }

    /* ANSI Escape code parsing */
    case '\033': {
      state.parsing_state = PRE_PARSING;
      return;
    }
  }

  if (state.parsing_state == PARSING && c != ';' && c != 'm') {
    state.nbr[state.ptr++] = c;
    return;
  }

  if (state.parsing_state == PRE_PARSING && c == '[') {
    state.parsing_state = PARSING;
    return;
  }

  if (state.parsing_state == PARSING && c == 'm') {
    int digit = atoi(state.nbr);
    state.parsing_state = OFF;

    if (digit == 0) {
      state.foreground = state.default_foreground;
      state.background = state.default_background;
    }

    if (digit > 29 && digit < 38) {
      state.foreground = state.palette[digit - 30];
    }

    state.ptr = 0;
    memset(state.nbr, 0, 3);

    return;
  }

  uint8_t *glyph = &fb_font.data[c * fb_font.glyph_size];
  size_t x = position_x * fb_font.width, y = position_y * fb_font.height;

  static const uint8_t masks[8] = {128, 64, 32, 16, 8, 4, 2, 1};

  size_t i, j;
  for (i = 0; i < fb_font.height; i++) {
    for (j = 0; j < fb_font.width; j++) {
      if (glyph[i] & masks[j]) {
        set_pixel(x + j, y + i, get_colour(&state.foreground));
      } else {
        set_pixel(x + j, y + i, get_colour(&state.background));
      }
    }
  }

  if (c != '\n') {
    state.cursor_x++;
  }
}

void text_put(char c) { text_putc(c, state.cursor_x, state.cursor_y); }

void text_puts(char *string) {
  while (*string) {
    text_putc(*string++, state.cursor_x, state.cursor_y);
  }
}

void text_printf(char *format, ...) {
  char buffer[1000] = {0};

  va_list args;
  va_start(args, format);

  vsprintf(buffer, format, args);

  va_end(args);

  text_puts(buffer);
}

void text_clear(void) { 
  set_screen(get_colour(&state.background));
  state.cursor_x = 1;
  state.cursor_y = 0;
}

void text_init(colour_t palette[8], colour_t foreground, colour_t background) {
  int i;
  for (i = 0; i < 8; i++) {
    state.palette[i] = palette[i];
  }

  state.default_foreground = foreground;
  state.foreground = foreground;
  state.default_background = background;
  state.background = background;

  state.cursor_x = 1;
  state.cursor_y = 0;

  log(INFO, "Text render initialized");
}
